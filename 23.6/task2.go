package main

import (
	"fmt"
	"strings"
)

func parseTest(sentences []string, chars []rune) (arr [][]int) {
	q := strings.Split(string(chars), "")

	for i := 0; i < len(sentences); i++ { // разобрали по предложениям
		s := strings.Split(sentences[i], " ")
		//	fmt.Println("Sentences: ", s)

		for j := len(s) - 1; j < len(s); j++ { // взяли последние слова
			//fmt.Println(s[j])
			ss := strings.Split(s[j], "")

			for z := 0; z < len(ss); z++ { // разобрали по буквам последние слова
				for k := 0; k < len(q); k++ { // разбор по буквам rune
					if ss[z] == q[k] {
						arr[i] = append(arr[i], k)
						//fmt.Println("1")
					} else {
						arr[i] = append(arr[i], -1)
					}
				}
				//fmt.Println(ss[z])
			}
		}
	}
	return
}

func main() {

	sentences := []string{"мама моет пол", "я ему суп", "папа молодец"}
	fmt.Println("Sentences: ", sentences)

	chars := []rune("пол")
	fmt.Println(string(chars))

	arr := make([][]int, len(chars))

	fmt.Println(parseTest(arr))

}
