package main

import (
	"fmt"
	"math/rand"
	"time"
)

func evenOddFunc(arr []int) (evenArr []int, oddArr []int) {
	for i := 0; i < len(arr); i++ {
		if arr[i]%2 == 0 {
			evenArr = append(evenArr, arr[i])
		} else {
			oddArr = append(oddArr, arr[i])
		}
	}
	return
}

func main() {

	rand.Seed(time.Now().UnixNano())

	arr := make([]int, 12)
	for i := 0; i < len(arr); i++ {
		arr[i] = rand.Intn(20)
	}
	fmt.Println("Ваш массив: ", arr)

	even, odd := evenOddFunc(arr)
	fmt.Println("Even array: ", even, "\n", "Odd array: ", odd)
}
