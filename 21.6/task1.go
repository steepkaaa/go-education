package main

import (
	"fmt"
)

func sumDiffTypes(x int16, y uint8, z float32) float32 {
	s := 2*float32(x) + float32(y)*float32(y) - 3/z
	return s

}
func main() {
	var x int16
	var y uint8
	var z float32
	fmt.Println("Введите х: ")
	fmt.Scan(&x)
	fmt.Println("Введите y: ")
	fmt.Scan(&y)
	fmt.Println("Введите z: ")
	fmt.Scan(&z)

	fmt.Println("S = ", sumDiffTypes(x, y, z))

}
