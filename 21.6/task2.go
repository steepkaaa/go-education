package main

import "fmt"

func yourFunc(A func(int, int) int) {
	var x, y int
	//x := 4
	//y := 3
	defer A(x, y)

}

func main() {
	//fmt.Println()
	x := 3
	y := 4

	f1 := yourFunc(func(x, y int) int { return x + y })
	f2 := yourFunc(func(x, y int) int { return x - y })
	f3 := yourFunc(func(x, y int) int { return x * y })

	fmt.Println(f1, f2, f3)

}
