package main

import (
	"fmt"
	"math"
)

func main() {

	//TASK 4
	fmt.Println("Приветствую тебя, садовод!")
	fmt.Println("----------------------------------------")

	var startHeight float64 = 100
	var growth float64 = 50
	var ate float64 = 20

	//1st day
	currentHeight := startHeight
	currentHeight += growth
	currentHeight -= ate

	//2nd day
	currentHeight += growth
	currentHeight -= ate

	//half 3rd day
	currentHeight += growth / 2

	//последовательное сложение
	fmt.Println(currentHeight, "см")

	// по формуле
	var height float64
	height = (growth-ate)*2 + growth/2 + startHeight
	fmt.Println(height, "см")

	//количество дней
	var days float64
	days = (300 - startHeight) / (growth - ate)
	fmt.Println("Количество неполных дней для достижения роста 3м: ", math.Round(days), "days")

	//разные гусеницы и разная скорость роста
	/*fmt.Println("Введите начальную выстоту саженца: см")
	var seedHeight int
	fmt.Scan(&seedHeight)

	fmt.Println("Введите скорость роста бамбука: см/сут")
	var growthSpeed int
	fmt.Scan(&growthSpeed)

	fmt.Println("Введите скорость поедания бамбука гусеницами: см/сут")
	var ateSpeed int
	fmt.Scan(&ateSpeed)

	fmt.Println("Введите количество дней")
	var daysPassed int
	fmt.Scan(&daysPassed)

	var newHeight int
	newHeight = (growthSpeed-ateSpeed)*daysPassed + seedHeight
	fmt.Println(newHeight, "см")

	//гарантированное созревание
	fmt.Println("Введите высоту зрелого бамбука: см")
	var finalHeight int
	fmt.Scan(&finalHeight)

	days = (finalHeight - startHeight) / (growth - ate)
	fmt.Println("Количество неполных дней для достижения высоты ", finalHeight, "см равно", days, "days")
	*/
}
