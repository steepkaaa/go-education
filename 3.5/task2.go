package main

import "fmt"

func main() {

	//TASK2
	fmt.Println("Приветствую тебя, пассажир!")
	fmt.Println("----------------------------------------")

	total := 0
	currentPass := 0
	fmt.Println("Прибываем на остановку Улица Программистов. В салоне пассажиров: ", currentPass)
	fmt.Println("Сколько пассажиров вышло на остановке? ", currentPass)
	fmt.Println("Сколько пассажиров зашло на остановке? ")
	var entered int
	comeOut := 0
	fmt.Scan(&entered)
	currentPass += entered - comeOut
	fmt.Println("Отправляемся с остановки Улица Программистов. В салоне пассажиров: ", currentPass)
	total += entered

	fmt.Println("Прибываем на остановку Проспект Алгоритмов. В салоне пассажиров: ", currentPass)
	fmt.Println("Сколько пассажиров вышло на остановке? ")
	fmt.Scan(&comeOut)
	fmt.Println("Сколько пассажиров зашло на остановке? ")
	fmt.Scan(&entered)
	currentPass += entered - comeOut
	fmt.Println("Отправляемся с остановки Проспект Алгоритмов. В салоне пассажиров: ", currentPass)
	total += entered

	fmt.Println("Прибываем на остановку Проспект Ломоносова. В салоне пассажиров: ", currentPass)
	fmt.Println("Сколько пассажиров вышло на остановке? ")
	fmt.Scan(&comeOut)
	fmt.Println("Сколько пассажиров зашло на остановке? ")
	fmt.Scan(&entered)
	currentPass += entered - comeOut
	fmt.Println("Отправляемся с остановки Проспект Ломоносова. В салоне пассажиров: ", currentPass)
	total += entered

	fmt.Println("Прибываем на остановку Проспект Гагарина. В салоне пассажиров: ", currentPass)
	fmt.Println("Сколько пассажиров вышло на остановке? ")
	fmt.Scan(&comeOut)
	fmt.Println("Сколько пассажиров зашло на остановке? ")
	fmt.Scan(&entered)
	currentPass += entered - comeOut
	fmt.Println("Отправляемся с остановки Проспект Гагарина. В салоне пассажиров: ", currentPass)
	total += entered

	fmt.Println("Всего заработали: ", total*20, " руб.")
	driverCount := 0
	driverCount = total * 20 / 4
	fmt.Println("Зарплата водителя: ", driverCount, " руб.")
	engineCount := 0
	engineCount = total * 20 / 5
	fmt.Println("Расходы на топливо: ", engineCount, " руб.")
	taxies := 0
	taxies = total * 20 / 5
	fmt.Println("Налоги: ", taxies, " руб.")
	repare := 0
	repare = total * 20 / 5
	fmt.Println("Ремонт машины: ", repare, " руб.")
	fmt.Println("Итого доход: ", total*20-driverCount-engineCount-taxies-repare, " руб.")
}
