package main

import "fmt"

func main() {
	//TASK 3
	a := 42

	b := 153

	/*var c int

	c = a
	a = b
	b = c */

	a, b = b, a

	fmt.Println("a:", a)

	fmt.Println("b:", b)

}
