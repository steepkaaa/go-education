package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"28.5/pkg/storage"
	"28.5/pkg/student"
)

func main() {

	var m = make(map[string]*student.Student)

	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Введите имя пользователя, возраст, курс через пробел: ")

	for scanner.Scan() {
		nameOfStudent := scanner.Text()
		s := strings.Fields(string(nameOfStudent))
		if len(s) == 3 {
			newName, age, grade := s[0], s[1], s[2]
			newAge, err := strconv.Atoi(age)
			if err != nil {
				panic(err)
			}
			newGrade, err := strconv.Atoi(grade)
			if err != nil {
				panic(err)
			}
			m[nameOfStudent] = &student.Student{
				Name:  newName,
				Age:   newAge,
				Grade: newGrade,
			}
		} else {
			fmt.Println("Error")
			break
		}
	}
	storage.StorageOfStudents(m)

}
