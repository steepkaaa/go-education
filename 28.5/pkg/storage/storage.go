package storage

import (
	"fmt"

	"28.5/pkg/student"
)

//m := make(map[string]*student.Student)
func StorageOfStudents(m map[string]*student.Student) {

	keys := []string{}
	for key := range m {
		keys = append(keys, key)
	}
	//return keys
	fmt.Println("Студенты из хранилища:\n", keys)
}
