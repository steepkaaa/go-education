package main

import (
	"fmt"
	"math"
)

func main() {

	var n int
	var x1, x2, y1, y2, x, y float64
	var sum float64 = 0

	fmt.Println("Введите число вершин: ")
	fmt.Scan(&n)

	fmt.Println("Введите x: ")
	fmt.Scan(&x)

	fmt.Println("Введите y: ")
	fmt.Scan(&y)

	x1 = x
	y1 = y

	for i := 0; i < n-1; i++ {
		fmt.Println("Введите x2: ")
		fmt.Scan(&x2)

		fmt.Println("Введите y2: ")
		fmt.Scan(&y2)

		sum += (x1 + x2) * (y2 - y1)
		x1 = x2
		y1 = y2
	}
	sum += (x + x2) * (y - y2)
	fmt.Println("Sum = ", math.Abs(sum/2))
}
