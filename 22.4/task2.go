package main

import (
	"errors"
	"fmt"
	"math/rand"
	"sort"
	"time"
)

func index(arr []int, k int) (int, error) {

	for i := 0; i < 12; i++ {
		if k == arr[i] {
			return i, nil
		}
	}
	return -1, errors.New("Ошибка")
}

func main() {

	rand.Seed(time.Now().UnixNano())

	var k int
	fmt.Println("Введите число: ")
	fmt.Scan(&k)

	arr := make([]int, 12)
	for i := 0; i < 12; i++ {
		arr[i] = rand.Intn(10)
	}
	sort.Ints(arr)
	fmt.Println("Ваш массив: ", arr)

	indexValue, err := index(arr, k)
	if err != nil {
		fmt.Println("В массиве нет числа: ", k)
		return
	}

	fmt.Println("Индекс введенного числа ", k, " : ", indexValue)
}
