package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

func findCount(arr []int, k int) (int, error) {
	for i := 0; i < 12; i++ {
		if arr[i] == k {
			count := len(arr) - i - 1
			return count, nil
		}
	}
	return -1, errors.New("Ошибка")
}

func main() {

	rand.Seed(time.Now().UnixNano())

	var k int
	fmt.Println("Введите число: ")
	fmt.Scan(&k)

	arr := make([]int, 12)
	for i := 0; i < 12; i++ {
		arr[i] = rand.Intn(10)
	}
	fmt.Println("Ваш массив: ", arr)

	indexValue, err := findCount(arr, k)
	if err != nil {
		fmt.Println("В массиве нет числа: ", k)
		return
	}
	fmt.Println("Число элементов после числа: ", k, " : ", indexValue)
}
