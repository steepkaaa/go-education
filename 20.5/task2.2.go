package main

import (
	"fmt"
	"math/rand"
	"time"
)

var aRows, aCols, bCols int

func multiArr(arr1 [][]int, arr2 [][]int) (arr3 [][]int) {

	for i := 0; i < aRows; i++ {
		for j := 0; j < bCols; j++ {
			for k := 0; k < aCols; k++ {
				arr3[i] = append(arr3[i], arr3[i][j]+arr1[i][k]*arr2[k][j])
			}
		}
	}
	return arr3
}

func main() {
	rand.Seed(time.Now().UnixNano())

	fmt.Println("Введите количество строк 1-ой матрицы: ")
	fmt.Scan(&aRows)

	fmt.Println("Введите количество столбцов 1-ой матрицы: ")
	fmt.Scan(&aCols)

	fmt.Println("Введите количество столбцов 2-ой матрицы: ")
	fmt.Scan(&bCols)

	arr1 := make([][]int, aRows)
	for i := 0; i < aRows; i++ {
		for j := 0; j < aCols; j++ {
			arr1[i] = append(arr1[i], rand.Intn(100))
		}
	}
	fmt.Println("1-ая матрица: ", arr1)

	arr2 := make([][]int, aRows)
	for i := 0; i < aRows; i++ {
		for j := 0; j < bCols; j++ {
			arr2[i] = append(arr2[i], rand.Intn(100))
		}
	}
	fmt.Println("2-ая матрица: ", arr2)

	fmt.Println("Результат перемножения матриц: ", multiArr(arr1, arr2))
}
