package main

import (
	"fmt"
)

const rows = 3
const cols = 3

func detArr(a [rows][cols]int) int {
	det := 0
	for i := 0; i < len(a); i++ {
		for j := 0; j < len(a[i]); j++ {
			det = a[0][0]*a[1][1]*a[2][2] +
				a[0][2]*a[1][0]*a[2][1] +
				a[2][0]*a[0][1]*a[1][2] -
				a[0][2]*a[1][1]*a[2][0] -
				a[0][0]*a[2][1]*a[1][2] -
				a[2][2]*a[1][0]*a[0][1]
		}
	}
	return det
}

func main() {

	arr := [rows][cols]int{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}
	fmt.Println(detArr(arr))
}
