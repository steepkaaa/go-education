package main

import (
	"fmt"
)

const aRows = 3
const aCols = 5
const bCols = 4

func multiArr(arr1 [aRows][aCols]int, arr2 [aCols][bCols]int) (arr3 [aRows][bCols]int) {
	for i := 0; i < aRows; i++ {
		for j := 0; j < bCols; j++ {
			for k := 0; k < aCols; k++ {
				arr3[i][j] = arr3[i][j] + arr1[i][k]*arr2[k][j]
			}
		}
	}
	return arr3
}

func main() {

	arr1 := [aRows][aCols]int{
		{1, 2, 3, 4, 5},
		{4, 5, 6, 1, 2},
		{7, 8, 9, 5, 6},
	}
	arr2 := [aCols][bCols]int{
		{1, 2, 3, 4},
		{4, 5, 6, 1},
		{7, 8, 9, 5},
		{7, 8, 9, 5},
		{7, 8, 9, 5},
	}
	fmt.Println(multiArr(arr1, arr2))
}
