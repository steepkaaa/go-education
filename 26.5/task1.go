package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Missing parameter, provide file name!")
		return
	}
	if len(os.Args) == 2 {
		data, err := ioutil.ReadFile(os.Args[1])
		if err != nil {
			fmt.Println("Can't read file:", os.Args[1])
			panic(err)
		}
		fmt.Println("File content is: ", string(data))
	}
	if len(os.Args) == 3 {
		data, err := ioutil.ReadFile(os.Args[1])
		if err != nil {
			fmt.Println("Can't read file:", os.Args[1])
			panic(err)
		}
		data1, err := ioutil.ReadFile(os.Args[2])
		if err != nil {
			fmt.Println("Can't read file:", os.Args[2])
			panic(err)
		}
		fmt.Println("first: ", string(data), "\n", "second: ", string(data1))
	}
	if len(os.Args) == 4 {
		data1, err := ioutil.ReadFile("first.txt")
		// Обработка ошибки
		if err != nil {
			fmt.Println(err)
		}

		data2, err := ioutil.ReadFile("second.txt")
		// Обработка ошибки
		if err != nil {
			fmt.Println(err)
		}

		// Объединить содержмиое файлов
		result := append(data1, data2...)

		err = ioutil.WriteFile("result.txt", result, 0777)
		// Обработка ошибки
		if err != nil {
			fmt.Println(err)
		}
	}
}
