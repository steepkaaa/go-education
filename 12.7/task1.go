package main

import (

	//"strings"
	"bufio"
	"fmt"
	"os"
	"time"
)

func main() {
	date := time.Now()

	fmt.Println("Введите сообщение: ")

	file, err := os.Create("filename.txt")

	if err != nil {
		fmt.Println("Ошибка в создании файла!", err)
		return
	}

	reader := bufio.NewReader(os.Stdin)
	str, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Ошбика ввода: ", err)
	}
	if str == "выход" {
		return
	}

	defer file.Close()

	file.WriteString(date.Format("2006-01-02 15:04:05 "))
	file.WriteString(str)

}
