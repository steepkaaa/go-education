package main

import (
	"fmt"
	"os"
)

func main() {

	//fmt.Println("Введите сообщение: ")

	file, err := os.Open("filename.txt")

	if err != nil {
		fmt.Println("Ошибка в открытии файла!", err)
		return
	}

	defer file.Close()

	buf := make([]byte, 128)
	_, err = file.Read(buf)
	//if _, err := io.ReadFull(file, buf); err != nil {
	if err != nil {
		fmt.Println("Не смогли прочитать последовательность байтов", err)
		return
	}
	fmt.Println(string(buf))

}
