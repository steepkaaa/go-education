package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {

	fmt.Println("Enter something text: ")

	reader := bufio.NewReader(os.Stdin)
	str, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}
	if str == "exit" {
		return
	}

	//write into file
	fileName := "text.txt"
	message := []byte(str)
	err = ioutil.WriteFile(fileName, message, 0777)
	if err != nil {
		panic(err)
	}

	//read from file
	content, err := ioutil.ReadFile(fileName)
	if err != nil {
		panic(err)
	}

	fmt.Printf("File contents: %s", content)

}
