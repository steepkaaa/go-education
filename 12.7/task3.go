package main

import (
	"fmt"
	"os"
)

func main() {

	f, err := os.Create("test.txt")
	if err != nil {
		fmt.Println("Ошибка в создании файла!", err)
		return
	}
	defer f.Close()

	// Получение текущих прав доступа у файла
	fi, err := f.Stat()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("Права доступа к файлу %v\n", fi.Mode())

	// Меняем права доступа файла
	err = f.Chmod(0444)
	if err != nil {
		fmt.Println(err)
	}
	fi, err = f.Stat()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("Права доступа к файлу %v\n", fi.Mode())

	f.WriteString("sssss")

}
