package main

import (
	"fmt"
)

func main() {

	fmt.Println("Счастливый билет")
	fmt.Println("-----------------")

	var a int

	fmt.Println("Введите 4х значное число: ")
	fmt.Scan(&a)

	if a/1000+((a%1000)/100) == (a%100)/10+a%10 {
		fmt.Println("Билет счастливый!")
		if a/1000 == a%10 && ((a%1000)/100) == (a%100)/10 {
			fmt.Println("Билет зеркальный")
		}
	} else {
		fmt.Println("Обычный билет")
	}
}
