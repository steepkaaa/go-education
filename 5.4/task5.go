package main

import (
	"fmt"
)

func main() {

	fmt.Println("Определение максимальных процентов")
	fmt.Println("-----------------")

	fmt.Println("Введите первую ставку")
	var x int
	fmt.Scan(&x)

	fmt.Println("Введите вторую ставку")
	var y int
	fmt.Scan(&y)

	fmt.Println("Введите третью ставку")
	var z int
	fmt.Scan(&z)

	/*if x > y && x > z {
		fmt.Println("Cтавки ", y, "% и", z, "% более выгодные")
	} else if y > x && y > z {
		fmt.Println("Cтавки ", x, "% и", z, "% более выгодные")
	} else {
		fmt.Println("Cтавки ", x, "% и", y, "% более выгодные")
	} */

	switch {
	case x > y && x > z:
		fmt.Println("Cтавки ", y, "% и", z, "% более выгодные")
	case y > x && y > z:
		fmt.Println("Cтавки ", x, "% и", z, "% более выгодные")
	default:
		fmt.Println("Cтавки ", x, "% и", y, "% более выгодные")
	}
}
