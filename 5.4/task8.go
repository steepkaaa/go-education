package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {

	fmt.Println("Игра “Угадывание числа” (дополнительно)")
	fmt.Println("-----------------")

	rand.Seed(time.Now().Unix())
	a := rand.Intn(10)
	fmt.Println(a, "Угадал?")

	for {
		var answer string
		fmt.Scan(&answer)

		if answer == "low" {
			a--
			fmt.Println(a, "Угадал?")
			continue
		}
		if answer == "high" {
			a++
			fmt.Println(a, "Угадал?")
			continue
		}

		fmt.Println("You guessed it!")
		break
	}
}
