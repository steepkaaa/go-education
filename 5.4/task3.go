package main

import (
	"fmt"
)

func main() {

	fmt.Println("Проверить, что есть совпадающие числа")
	fmt.Println("-----------------")

	fmt.Println("Введите первое число")
	var x int
	fmt.Scan(&x)

	fmt.Println("Введите второе число")
	var y int
	fmt.Scan(&y)

	fmt.Println("Введите третье число")
	var z int
	fmt.Scan(&z)

	if x == y || x == z || y == z {
		fmt.Println("Есть совпадения")
	} else {
		fmt.Println("Совпадений нет")
	}
}
