package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {

	fmt.Println("Игра “Угадывание числа” (дополнительно)")
	fmt.Println("-----------------")

	fmt.Println("Введите min: ")
	var min int
	fmt.Scan(&min)

	fmt.Println("Введите max: ")
	var max int
	fmt.Scan(&max)

	fmt.Println("Введите число попыток")
	var k int
	fmt.Scan(&k)

	rand.Seed(time.Now().UTC().UnixNano())
	a := randInt(min, max)

	if k < 0 {
		fmt.Println("k > 0 !!!")
	} else {
		fmt.Println("Угадал? ", a)
	}

	for i := 0; i < k; i++ {

		var answer string
		fmt.Scan(&answer)

		/*if answer == "no" {
			a := randInt(min, max)
			fmt.Println(a, "- Угадал?")
			continue
		}
		fmt.Println("You guessed it!")
		break */

		switch {
		case answer == "no":
			a := randInt(min, max)
			fmt.Println(a, "- Угадал?")
			continue
		case i == k:
			fmt.Println("Не угадал")
		default:
			fmt.Println("You guessed it!")
		}
	}
}
func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}
