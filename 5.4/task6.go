package main

import (
	"fmt"
	"math"
)

func main() {

	fmt.Println("Решение квадратного уравнения")
	fmt.Println("-----------------")

	fmt.Println("Введите a")
	var a, b, c float64
	fmt.Scan(&a)

	fmt.Println("Введите b")
	fmt.Scan(&b)

	fmt.Println("Введите c")
	fmt.Scan(&c)

	d := b*b - 4*a*c

	/*if d > 0 {
		x1 := (-b + math.Sqrt(math.Pow(d, 2))) / (2 * a)
		x2 := (-b - math.Sqrt(math.Pow(d, 2))) / (2 * a)
		fmt.Println("x1 = ", x1, "x2 = ", x2)
	} else if d == 0 {
		x1 := -b / (2 * a)
		fmt.Println("x = ", x1)
	} else {
		fmt.Println("Корней нет")
	} */

	switch {
	case d > 0:
		x1 := (-b + math.Sqrt(math.Pow(d, 2))) / (2 * a)
		x2 := (-b - math.Sqrt(math.Pow(d, 2))) / (2 * a)
		fmt.Println("x1 = ", x1, "x2 = ", x2)
	case d == 0:
		x1 := -b / (2 * a)
		fmt.Println("x = ", x1)
	default:
		fmt.Println("Корней нет")
	}
}
