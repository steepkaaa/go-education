package main

import (
	"fmt"
)

func main() {

	fmt.Println("Определение координатной плоскости точки")
	fmt.Println("-----------------")

	fmt.Println("Введите координату X")
	var x int
	fmt.Scan(&x)

	fmt.Println("Введите координату У")
	var y int
	fmt.Scan(&y)

	/*if x > 0 && y > 0 {
		fmt.Println("Первая четверть")
	} else if x < 0 && y > 0 {
		fmt.Println("Вторая четверть")
	} else if x < 0 && y < 0 {
		fmt.Println("Третья четверть")
	} else {
		fmt.Println("Четвертая четверть")
	} */

	switch {
	case x > 0 && y > 0:
		fmt.Println("Первая четверть")
	case x < 0 && y > 0:
		fmt.Println("Вторая четверть")
	case x < 0 && y < 0:
		fmt.Println("Третья четверть")
	default:
		fmt.Println("Четвертая четверть")
	}

}
