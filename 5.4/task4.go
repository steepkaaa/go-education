package main

import (
	"fmt"
)

func main() {

	fmt.Println("Сумма без сдачи")
	fmt.Println("-----------------")

	fmt.Println("Введите стоимость товара:")
	var cost int
	fmt.Scan(&cost)

	fmt.Println("Введите номинал 1-ой:")
	var coin1 int
	fmt.Scan(&coin1)

	fmt.Println("Введите номинал 2-ой:")
	var coin2 int
	fmt.Scan(&coin2)

	fmt.Println("Введите номинал 3-ей:")
	var coin3 int
	fmt.Scan(&coin3)

	/*if cost == (coin1 + coin2 + coin3) {
		fmt.Println("Пользователь может оплатить без сдачи (3)")
	} else if cost == (coin1+coin2) || cost == (coin2+coin3) || cost == (coin1+coin3) {
		fmt.Println("Пользователь может оплатить без сдачи (2)")
	} else if cost == coin1 || cost == coin2 || cost == coin3 {
		fmt.Println("Пользователь может оплатить без сдачи (1)")
	} else {
		fmt.Println("Пользователь не может оплатить без сдачи")
	} */

	switch {
	case cost == (coin1 + coin2 + coin3):
		fmt.Println("Пользователь может оплатить без сдачи (3)")
	case cost == (coin1+coin2) || cost == (coin2+coin3) || cost == (coin1+coin3):
		fmt.Println("Пользователь может оплатить без сдачи (2)")
	case cost == coin1 || cost == coin2 || cost == coin3:
		fmt.Println("Пользователь может оплатить без сдачи (1)")
	default:
		fmt.Println("Пользователь не может оплатить без сдачи")
	}

}
