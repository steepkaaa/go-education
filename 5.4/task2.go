package main

import (
	"fmt"
)

func main() {

	fmt.Println("Проверить, что одно из чисел положительное")
	fmt.Println("-----------------")

	fmt.Println("Введите первое число")
	var x int
	fmt.Scan(&x)

	fmt.Println("Введите второе число")
	var y int
	fmt.Scan(&y)

	fmt.Println("Введите третье число")
	var z int
	fmt.Scan(&z)

	if x > 0 || y > 0 || z > 0 {
		fmt.Println("Есть положительное число (числа)")
	} else {
		fmt.Println("Все числа отрицательные")
	}
}
