package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Student struct {
	name  string
	age   int
	grade int
}

func main() {

	m := make(map[string]*Student)
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Введите имя пользователя, возраст, курс через пробел: ")

	for scanner.Scan() {
		//fmt.Println("Введите имя пользователя, возраст, курс через пробел: ")

		nameOfStudent := scanner.Text()

		s := strings.Fields(string(nameOfStudent))

		if len(s) == 3 {
			newName, age, grade := s[0], s[1], s[2]
			newAge, err := strconv.Atoi(age)
			if err != nil {
				panic(err)
			}
			newGrade, err := strconv.Atoi(grade)
			if err != nil {
				panic(err)
			}
			m[nameOfStudent] = &Student{
				name:  newName,
				age:   newAge,
				grade: newGrade,
			}
		} else {
			fmt.Println("Error")
			break
		}
	}

	//if err := scanner.Err(); err == io.EOF {
	//	fmt.Println("Студенты из хранилища: ")
	keys := []string{}
	for key := range m {
		keys = append(keys, key)
	}
	fmt.Println("Студенты из хранилища:\n", keys)

}
