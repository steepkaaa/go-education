package main

import (
	"fmt"
	"math/rand"
	"time"
)

func sortBubleReverse(arr []int) []int {
	for i := 0; i < len(arr); i++ {
		for j := i; j < len(arr); j++ {
			if arr[i] < arr[j] {
				arr[i], arr[j] = arr[j], arr[i]
			}
		}
	}
	return arr
}

func main() {

	rand.Seed(time.Now().UnixNano())

	arr := make([]int, 12)
	for i := 0; i < len(arr); i++ {
		arr[i] = rand.Intn(20)
	}
	fmt.Println("Ваш массив: ", arr)
	fmt.Println("Отсортированный наоборот: ", sortBubleReverse(arr))

}
