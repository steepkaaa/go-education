package main

import (
	"fmt"
	"math/rand"
	"time"
)

func sortInsert(arr []int) []int {
	for i := 0; i < len(arr); i++ {
		x := arr[i]
		j := i
		for ; j >= 1 && arr[j-1] > x; j-- {
			arr[j] = arr[j-1]
		}
		arr[j] = x
	}
	return arr
}

func main() {

	rand.Seed(time.Now().UnixNano())

	arr := make([]int, 12)
	for i := 0; i < len(arr); i++ {
		arr[i] = rand.Intn(20)
	}
	fmt.Println("Ваш массив: ", arr)
	//	sortInsert(arr)
	fmt.Println("Отсортированный массив: ", sortInsert(arr))

}
