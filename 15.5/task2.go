package main

import (
	"fmt"
)

func reverse(arr []int, n int) (reverseArr []int) {
	j := 0
	//n := len(arr)
	reverseArr = make([]int, n)
	for i := len(arr) - 1; i >= 0; i-- {
		reverseArr[j] = arr[i]
		j++
	}
	return
}
func main() {

	var n int
	fmt.Println("Введите количество элементов в срезе: ")
	err, _ := fmt.Scan(&n)
	if n <= 1 {
		panic(err)
	}

	arr := make([]int, n)

	for i := 0; i < n; i++ {
		fmt.Println("Введите значения элемента: ")
		fmt.Printf("%v - ый элемент: ", i)
		fmt.Scan(&arr[i])
	}
	fmt.Println("Ваш массив: ", arr)

	fmt.Println("Реверсивный массив: ", reverse(arr, n))

}
