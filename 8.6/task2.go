package main

import "fmt"

func main() {

	s := make([]string, 5)

	s[0] = "Понедельник"
	s[1] = "Вторник"
	s[2] = "Среда"
	s[3] = "Четверг"
	s[4] = "Пятница"

	fmt.Println("Рабочая неделя:", s)

	var day string
	fmt.Println("Введите день: ")
	fmt.Scan(&day)

	switch day {
	case "пн":
		//l := s[0:5]
		fmt.Println("Осталось работать: ", s)

	case "вт":
		l := s[1:5]
		fmt.Println("Осталось работать: ", l)

	case "ср":
		l := s[2:5]
		fmt.Println("Осталось работать: ", l)

	case "чт":
		l := s[3:5]
		fmt.Println("Осталось работать: ", l)

	case "пт":
		l := s[4:5]
		fmt.Println("Осталось работать: ", l)

	default:
		fmt.Println("Некорректный ввод")
	}
}
