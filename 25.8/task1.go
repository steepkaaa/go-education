package main

import (
	"flag"
	"fmt"
)

func containString(str string, substr string) bool {
	res := false
	if len(str) == 0 {
		fmt.Println("Строка неверно задана")
	} else if len(substr) == 0 {
		fmt.Println("Подстрока неверно задана")
	} else {
		for i := 0; i < len(str)-len(substr); i++ {
			for j := 0; j < len(substr); j++ {
				if substr[j] != str[i+j] {
					break
				} else if j == len(substr)-1 {
					res = true
					break
				}
			}
		}
	}
	return res
}
func main() {

	var str = flag.String("str", "q", "Input str")
	var substr = flag.String("substr", "w", "Input substr")
	flag.Parse()

	fmt.Println(containString(*str, *substr))

}
