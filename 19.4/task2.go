package main

import (
	"fmt"
	//"math/rand"
	//	"time"
)

func BubbleSort(arr []int) {
	for i := 0; i < len(arr); i++ {
		for j := i; j < len(arr); j++ {
			if arr[i] > arr[j] {
				swap(arr, i, j)
			}
		}
	}
}

func swap(ar []int, i, j int) {
	tmp := ar[i]
	ar[i] = ar[j]
	ar[j] = tmp
}

func main() {

	arr := []int{1, 2, 3, 4, 5}
	BubbleSort(arr)

	fmt.Println("Отсортированный массив: ", arr)

}
