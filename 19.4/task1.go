package main

import (
	"fmt"
	"sort"
)

func arrAdd(arr1 []int, arr2 []int) []int {
	arr1 = append(arr1, arr2...)
	sort.Ints(arr1)
	newArr := arr1
	return newArr

}
func main() {

	arr1 := []int{9, 8, 7, 6, 5}
	arr2 := []int{1, 2, 3, 4}

	newArr := arrAdd(arr1, arr2)
	fmt.Println(newArr)

}
