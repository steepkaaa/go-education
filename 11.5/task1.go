package main

import (
	"fmt"
	"strings"
)

func main() {

	a := "Go is an Open source programming Language that makes it Easy to build simple, reliable, and efficient Software"
	count := 0
	// s := strings.Trim(a, " ")
	space := " "

	for strings.Contains(a, space) {
		spaceIndex := strings.Index(a, space)
		word := a[:spaceIndex]

		if word == strings.Title(word) {
			count++
			fmt.Println(word)
		}

		a = a[spaceIndex+1:]
		a = strings.Trim(a, space)
	}

	if a == strings.Title(a) {
		count++
		fmt.Println(a)
	}
	fmt.Println("Слов, начинающихся с большой буквы: ", count)
}
