package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {

	a := "a10 10 20b 20 30c30 30 dd"
	//count := 0
	// s := strings.Trim(a, " ")
	space := " "

	for strings.Contains(a, space) {
		spaceIndex := strings.Index(a, space)
		word := a[:spaceIndex]

		i, err := strconv.Atoi(word)
		if err == nil {
			fmt.Println(i)
		}

		a = a[spaceIndex+1:]
		a = strings.Trim(a, space)
	}

	i, err := strconv.Atoi(a)
	if err == nil {
		fmt.Println(i)
	}
}
