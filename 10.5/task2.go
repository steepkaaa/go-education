package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("Введите сумму: ")
	var x float64
	fmt.Scan(&x)

	fmt.Println("Введите количество лет: ")
	var year int
	fmt.Scan(&year)

	fmt.Println("Введите %: ")
	var p float64
	fmt.Scan(&p)

	var result float64

	for i := 1; i <= year*12; i++ {
		x += x * 0.01 * p
		result += x - math.Floor(x*100)/100
	}

	fmt.Println("Конечная сумма: ", math.Floor(x*100)/100)

	fmt.Println("Банк получит: ", result)

}
