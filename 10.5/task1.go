package main

import (
	"fmt"
	"math"
)

func fac(k int) int { //считаем факториал

	if k == 0 {
		return 1
	}
	return k * fac(k-1)
}

func main() {

	fmt.Println("Введите точность(до какого знака): ")
	var n int
	fmt.Scan(&n)

	epsilon := float64(1 / math.Pow(10, float64(n)))

	fmt.Println("Введите x: ")
	var x float64
	fmt.Scan(&x)

	result := 0.0
	prevResult := 0.0
	k := 0

	for {
		result += (math.Pow(x, float64(k))) / float64(fac(k))
		if math.Abs(result-prevResult) < epsilon {
			fmt.Println(result)
			break
		}
		k++
		prevResult = result
	}

}
