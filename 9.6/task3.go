package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {

	rand.Seed(time.Now().UTC().UnixNano())
	fmt.Println("Введите количество адресов: ")
	var count int
	fmt.Scan(&count)
	fmt.Println("=======================\n")
	for i := 0; i < count; i++ {
		fmt.Println(rand.Intn(255), ".", rand.Intn(255), ".", rand.Intn(255), ".", rand.Intn(255))
	}
}
