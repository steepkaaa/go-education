package main

import (
	"fmt"
	"math"
	"unsafe"
)

/*const (
	//MaxInt8   = 1<<7 - 1
	MinInt8 = -1 << 7
	//	MaxInt16  = 1<<15 - 1
	MinInt16 = -1 << 15
	//	MaxInt32  = 1<<31 - 1
	MinInt32 = -1 << 31
	//	MaxInt64  = 1<<63 - 1
	//	MinInt64  = -1 << 63
	MaxUint8  = 1<<8 - 1
	MaxUint16 = 1<<16 - 1
	MaxUint32 = 1<<32 - 1
	//MaxUint64 = 1<<64 - 1
) */

func main() {

	var a, b int
	fmt.Println("Введите первое число: ")
	fmt.Scan(&a)

	fmt.Println("Введите второе число: ")
	fmt.Scan(&b)

	switch {
	case a*b > 0 && a*b <= math.MaxUint8:
		c := uint8(a * b)
		fmt.Printf("Значение: %v Размер: %d байт Тип: %T\n", c, unsafe.Sizeof(c), c)
	case a*b < 0 && a*b >= math.MinInt8:
		c := int8(a * b)
		fmt.Printf("Значение: %v Размер: %d байт Тип: %T\n", c, unsafe.Sizeof(c), c)
	case a*b > 0 && a*b <= math.MaxUint16 && a*b > math.MaxUint8:
		c := uint16(a * b)
		fmt.Printf("Значение: %v Размер: %d байт Тип: %T\n", c, unsafe.Sizeof(c), c)
	case a*b < 0 && a*b >= math.MinInt16 && a*b < math.MinInt8:
		c := int16(a * b)
		fmt.Printf("Значение: %v Размер: %d байт Тип: %T\n", c, unsafe.Sizeof(c), c)
	case a*b > 0 && a*b <= math.MaxUint32 && a*b > math.MaxUint16:
		c := uint32(a * b)
		fmt.Printf("Значение: %v Размер: %d байт Тип: %T\n", c, unsafe.Sizeof(c), c)
	case a*b < 0 && a*b >= math.MinInt32 && a*b < math.MinInt16:
		c := int32(a * b)
		fmt.Printf("Значение: %v Размер: %d байт Тип: %T\n", c, unsafe.Sizeof(c), c)
	default:
		fmt.Printf("Значение: %v Размер: %d байт Тип: %T\n", a*b, unsafe.Sizeof(a*b), a*b)
	}
}
