package main

import (
	"fmt"
	"math"
)

/*const (
	MaxUint8  = 1<<8 - 1
	MaxUint16 = 1<<16 - 1
	MaxUint32 = 1<<32 - 1
) */

func main() {

	k := 0
	l := 0

	for i := math.MaxUint8; i <= math.MaxUint32; i++ {
		if i%math.MaxUint8 == 0 {
			k++
		}
		if i%math.MaxUint16 == 0 {
			l++
		}
	}
	fmt.Println("Переполнений uint8 в uint32: ", k)
	fmt.Println("Переполнений uint16 в uint32: ", l)
}
