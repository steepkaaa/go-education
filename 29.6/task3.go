package main

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func main() {

	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	wg := &sync.WaitGroup{}

	go sqr(sigs, done, wg)

	fmt.Println("awaiting signal")
	<-done

	fmt.Println("exiting")
}

func sqr(sigs chan os.Signal, done chan bool, wg *sync.WaitGroup) {

	wg.Add(1)
	var i int
	for {
		i++
		select {
		case <-sigs:
			fmt.Println()
			fmt.Println("Выхожу из программы...")
			done <- true
		default:
			result := i * i
			time.Sleep(time.Millisecond * 200)

			fmt.Printf("Значение %v, его квадрат равен %v\n", i, result)
			//wg.Wait()
		}
	}
}
