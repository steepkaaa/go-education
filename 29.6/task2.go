package main

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func initGracefulShutdown(cancelFunc context.CancelFunc, wg *sync.WaitGroup) {
	defer wg.Done()
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGILL)
	<-sigs
	fmt.Println("Graceful Shutdown")
	cancelFunc()
}

func action(cancelCtx context.Context) error {
	time.Sleep(100 * time.Millisecond)
	return errors.New("failed")
}

func main() {
	cancelCtx, cancelFunc := context.WithCancel(context.Background())

	go func() {
		err := action(cancelCtx)

		if err != nil {
			cancelFunc()
		}
	}()

	squadChan := make(chan int)

	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

	shutdownChan := make(chan string)

	wg := &sync.WaitGroup{}
	wg.Add(1)

	go func(shutdown chan string, wg *sync.WaitGroup) {
		i := 1

		for {
			time.Sleep(time.Second)
			i++
			select {
			case <-c:
				fmt.Println("Выхожу из программы... ")
				wg.Done()
				return
			default:
				squadChan <- i * i
				fmt.Println(<-squadChan)
			}
		}
	}(shutdownChan, wg)

	go initGracefulShutdown(cancelFunc, wg)
	wg.Wait()
}
