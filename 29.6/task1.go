package main

import (
	"fmt"
	"strconv"
)

/*func squad(newK int, ch chan int) {
	ch <- newK * newK
} */

func squad(inputCh chan int) chan int {
	squadChan := make(chan int)
	val := <-inputCh

	squadChan <- val * val

	return squadChan
}

/*func multy(data int, ch chan int) {
	data = data * 2
	ch <- data
} */
func main() {
	for {
		var k string
		fmt.Println("Введите число: ")
		fmt.Scan(&k)

		if k == "stop" {
			return
		}

		newK, err := strconv.Atoi(k)
		if err != nil {
			panic(err)
		}
		squadChan := make(chan int)
		squadChan <- newK

		go squad(squadChan)

		fmt.Println("Квадрат: ", <-squadChan)

		//intCh := make(chan int)

		/*go squad(newK, intCh)
		data := <-intCh
		fmt.Println("Квадрат: ", data) */

		/*go multy(data, intCh)

		fmt.Println("Умножение: ", <-intCh) */
	}
}
