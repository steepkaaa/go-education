package main

import (
	"fmt"
)

func main() {
	result()
}
func result() {
	var total, sale int
	fmt.Println("Enter Total: ")
	fmt.Scan(&total)

	fmt.Println("Enter Sale %: ")
	fmt.Scan(&sale)

	sizeOfSale := total * sale / 100
	if sale > 30 || sizeOfSale > 2000 {
		fmt.Println("Discount limit exceeded")
		return
	}
	fmt.Println("Your sale is: ", sizeOfSale)
}
