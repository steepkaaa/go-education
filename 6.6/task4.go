package main

import (
	"fmt"
)

func main() {
	j := 0
	i := 0
	k := 0
	state := 1
	isLimit1 := false
	isLimit2 := false
	isLimit3 := false

	for {
		if isLimit1 == false {
			fmt.Println("i: ", i)
			i++
			if i > 5 {
				isLimit1 = true
				state ++
			}
		}
		if isLimit2 == false && state == 2 {
			fmt.Println("j: ", j)
			j++
			if j > 10 {
				isLimit2 = true
				state ++
			}
		}
		if isLimit3 == false && state == 3 {
			fmt.Println("k: ", k)
			k++
			if k > 15 {
				isLimit3 = true
			}
		}
		if isLimit1 == true && isLimit2 == true && isLimit3 == true {
			break
		}
	
	}
}