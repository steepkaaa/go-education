package main

import (
	"fmt"
)

func main() {
	var a, b int
	fmt.Println("Enter a: ")
	fmt.Scan(&a)

	fmt.Println("Enter b: ")
	fmt.Scan(&b)

	for i := a; i <= (a + b); i++ {
		fmt.Println("Sum = ", i)
	}
}
