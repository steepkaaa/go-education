package main

import (
	"fmt"
)

func main() {

	var count1, count2, count3 int
	fmt.Println("Введите лимит 1-ой корзины: ")
	fmt.Scan(&count1)

	fmt.Println("Введите лимит 2-ой корзины: ")
	fmt.Scan(&count2)

	fmt.Println("Введите лимит 3-ей корзины: ")
	fmt.Scan(&count3)

	for i := 0; i <= count1; i++ {
		fmt.Println("Количество в 1-ой: ", i)
		if i == count1 {
			for j := 0; j <= count2; j++ {
				fmt.Println("Количество во 2-ой: ", j)
				if j == count2 {
					for k := 0; k <= count3; k++ {
						fmt.Println("Количество в 3-ей: ", k)
					}
				}
			}
		}
	}
}
