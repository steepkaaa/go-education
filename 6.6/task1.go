package main

import (
	"fmt"
)

func main() {
	var a int
	fmt.Println("Enter any number: ")
	fmt.Scan(&a)

	for i := 0; i <= a; i++ {
		fmt.Println(i)
	}
}
