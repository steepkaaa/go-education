package main

import "fmt"

func main() {

	k := 0
	for i := 100000; i <= 999999; i++ {
		if i/100000 == i%10 && ((i % 100000)/10000) == (i%100)/10 && (i % 10000)/1000 == (i%1000)/100 {
		k++
		} 
	}
	fmt.Println(k)
}
