package main

import (
	"fmt"
)

func main() {
lastTiket := 0
max := 0

for i := 100000; i <= 999999; i++ {
    if i/100000 + ((i % 100000)/10000) + (i % 10000) /1000 == i%10 + ((i%100)/10) + ((i%1000)/100) {
      if lastTiket == 0 {
          lastTiket = i
      } else {
        if i-lastTiket > max {
          max = i - lastTiket
        }
       lastTiket = i
      }
    }  
}
fmt.Println("Максимальное число билетов необходимо: ",max)
}
