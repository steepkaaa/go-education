package main

import (
	"fmt"
)

func main() {
	var x int
    fmt.Println("Введите кол-во полей по горизонтали: ")
    fmt.Scan(&x)

    var y int
    fmt.Println("Введите кол-во полей по вертикали: ")
    fmt.Scan(&y)

	fmt.Println("------------------------- ")
    for k := 0; k < y; k++ {
    	if k % 2 == 0 {
      		for i := 0; i < x; i ++ {
        		if i % 2 == 0 {
        			fmt.Printf("*")
        		} else {
        			fmt.Printf(" ")
        		}
      		}
    	} else {
  
    fmt.Printf("\n")

    		for ii := 0; ii < x; ii ++ {
      			if ii % 2 == 0 {
        			fmt.Printf(" ")
      			} else {
        			fmt.Printf("*")
      			}
    		}
    fmt.Printf("\n")
  		}
    } 
}