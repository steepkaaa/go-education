package main

import (
	"fmt"
)

func main() {

	fmt.Println("Банкомат")
	fmt.Println("-----------------")

	fmt.Println("Введите сумму, которую хотите снять: ")
	var a int32
	fmt.Scan(&a)

	if a > 100 && a < 100000 {
		if a%100 == 0 {
			fmt.Println("Вы можете снять ", a, "руб.")
		} else {
			fmt.Println("Данная сумма ", a, "руб. не может быть снята")
		}
	} else {
		fmt.Println("Данная сумма ", a, "руб. не может быть снята")
	}
}
