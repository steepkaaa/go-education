package main

import (
	"fmt"
	"math/rand"
)

func main() {

	fmt.Println("Студенты")
	fmt.Println("-----------------")

	fmt.Println("Введите число студентов: ")
	var students int
	fmt.Scan(&students)

	fmt.Println("Введите число групп: ")
	var groups int
	fmt.Scan(&groups)

	fmt.Println("Введите порядковый номер студента: ")
	var number int
	fmt.Scan(&number)

	if number > students {
		fmt.Println("Порядковый номер больше, чем количество студентов!!!")
	} else {
		fmt.Println("Студент под номером: ", number, "пойдет в группу: ", rand.Intn(groups))
	}
}
