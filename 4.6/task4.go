package main

import (
	"fmt"
)

func main() {

	fmt.Println("Три числа vol.2")
	fmt.Println("-----------------")

	fmt.Println("Введите первое ")
	var a int
	fmt.Scan(&a)

	fmt.Println("Введите второе ")
	var b int
	fmt.Scan(&b)

	fmt.Println("Введите третье ")
	var c int
	fmt.Scan(&c)

	var count int

	if a >= 5 && b < 5 && c < 5 {
		count = 1
		fmt.Println(count)
	} else if a < 5 && b >= 5 && c < 5 {
		count = 1
		fmt.Println(count)
	} else if a < 5 && b < 5 && c >= 5 {
		count = 1
		fmt.Println(count)
	} else if a >= 5 && b >= 5 && c < 5 {
		count = 2
		fmt.Println(count)
	} else if b >= 5 && c >= 5 && a < 5 {
		count = 2
		fmt.Println(count)
	} else if a >= 5 && c >= 5 && b < 5 {
		count = 2
		fmt.Println(count)
	} else if a >= 5 && b >= 5 && c >= 5 {
		count = 3
		fmt.Println(count)
	} else {
		fmt.Println("Нет чисел, > 5")
	}
}
