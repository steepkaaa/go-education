package main

import (
	"fmt"
)

func main() {

	fmt.Println("Сдача ЕГЭ")
	fmt.Println("-----------------")

	fmt.Println("Введите количество баллов за 1 экзамен")
	var exam1 int
	fmt.Scan(&exam1)

	fmt.Println("Введите количество баллов за 2 экзамен")
	var exam2 int
	fmt.Scan(&exam2)

	fmt.Println("Введите количество баллов за 3 экзамен")
	var exam3 int
	fmt.Scan(&exam3)

	sum := exam1 + exam2 + exam3

	if sum >= 275 {
		fmt.Println("Поздравляю, Вы поступили!")
	} else {
		fmt.Println("Попробуй через год или выберите другой универ.")
	}
}
