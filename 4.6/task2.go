package main

import (
	"fmt"
)

func main() {

	fmt.Println("3 числа")
	fmt.Println("-----------------")

	fmt.Println("Введите 1-е число")
	var a int
	fmt.Scan(&a)

	fmt.Println("Введите 2-е число")
	var b int
	fmt.Scan(&b)

	fmt.Println("Введите 3-е число")
	var c int
	fmt.Scan(&c)

	if a > 5 || b > 5 || c > 5 {
		fmt.Println("Есть число (числа) > 5")
	} else {
		fmt.Println("Таких чисел")
	}
}
