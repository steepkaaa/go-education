package main

import (
	"fmt"
)

func main() {

	fmt.Println("Прогрессивный налог")
	fmt.Println("-----------------")

	fmt.Println("Введите Вашу з/п ")
	var salary float32
	fmt.Scan(&salary)

	var tax float32

	if salary >= 50000 {
		tax = (salary-50000)*0.3 + 9300
	} else if salary > 10000 && salary < 50000 {
		tax = (salary-10000)*0.2 + 1300
	} else {
		tax = salary * 0.13
	}
	fmt.Println("Ваш налог равен ", tax, "руб.")
}
