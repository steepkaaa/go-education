package main

import (
	"fmt"
)

func main() {

	fmt.Println("Ресторан")
	fmt.Println("-----------------")

	fmt.Println("Какой сегодня день? ")
	var day string
	fmt.Scan(&day)

	fmt.Println("Сколько вас? ")
	var guests int
	fmt.Scan(&guests)

	fmt.Println("Какова сумма чека? ")
	var sum float32
	fmt.Scan(&sum)

	var total float32

	if day == "monday" && guests < 5 {
		total = sum * 90 / 100
	} else if day == "monday" && guests > 5 {
		total = sum * 80 / 100
	} else if day == "friday" && sum > 10000 && guests < 5 {
		total = sum * 95 / 100
	} else if day == "friday" && sum > 10000 && guests > 5 {
		total = sum * 105 / 100
	} else if guests > 5 {
		total = sum * 110 / 100
	} else {
		total = sum
	}
	fmt.Println("Ваш счет составляет ", total, "руб.")
}
