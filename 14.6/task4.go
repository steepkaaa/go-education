package main

import (
	"fmt"
	//"math/rand"
	//	"time"
)

const a = 2
const b = 3
const c = 4

func f1(x int) (y int) {
	y = x + a
	return
}
func f2(x int) (y int) {
	y = x + b
	return
}
func f3(x int) (y int) {
	y = x + c
	return
}

func main() {

	fmt.Println(f1(f2(f3(1))))

}
