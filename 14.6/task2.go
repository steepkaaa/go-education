package main

import (
	"fmt"
	"math/rand"
	"time"
)

func getRandonPoint(n1, n2 int) (int, int) {
	return rand.Intn(n1), rand.Intn(n2)
}
func solve(x, y int) {
	fmt.Println(2*x+10, -3*y-5)
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	x1, y1 := getRandonPoint(10, 20)
	x2, y2 := getRandonPoint(10, 20)
	x3, y3 := getRandonPoint(10, 20)

	fmt.Println("Начальные координаты: ")

	fmt.Println(x1, y1)
	fmt.Println(x2, y2)
	fmt.Println(x3, y3)

	fmt.Println("После преобразования: ")

	solve(x1, y1)
	solve(x2, y2)
	solve(x3, y3)
}
