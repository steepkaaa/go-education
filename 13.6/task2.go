package main

import "fmt"

func fib_n(n int) int {
	//F(n)
	x := 1
	//F(n-1)
	y := 0
	for i := 0; i < n; i++ {
		x += y
		y = x - y
	}
	fmt.Println(y)
	return y
}

func main() {
	fmt.Println("enter N: ")
	var n int
	fmt.Scan(&n)

	fib_n(n)
}
