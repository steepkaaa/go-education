package main

import "fmt"

func firstWord() {

	fmt.Println("Hello ")
}
func secondWord() {
	fmt.Println("World!")
}
func fullPhrase(firstWord func(), secondWord func()) {
	secondWord()
	firstWord()
}

func main() {

	fullPhrase(firstWord, secondWord)

}
